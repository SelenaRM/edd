package ColaDePrioridad;

public class Dias {
    String[] horarios;
    private int dia;

    //private int contadorDia = 0;
    public Dias(){
        
    }

    public Dias(int dia) {
        horarios = new  String[12];
        horarios[0] = "8:00";
        horarios[1] = "8:30";
        horarios[2] = "9:00";
        horarios[3] = "9:30";
        horarios[4] = "10:00";
        horarios[5] = "10:30";
        horarios[6] = "11:00";
        horarios[7] = "11:30";
        horarios[8] = "12:00";
        horarios[9] = "12:30";
        horarios[10]= "13:00";
        horarios[11]= "13:30";
    }

   public int diasMesJunio(int dia) {
        int n = 0;
        if (dia == 6 || dia == 13 || dia == 20 || dia == 27) {
            n = 0;
        } else {
            n = dia;
        }
        return n;
    }
    /*public void horarios(int pC, int pDia, Consultorio consultorios[]) {
        System.out.println(" -------------- HORARIOS DISPONIBLES -------------");
        for(int i=0; i<12;i++){
            if(consultorios[pC].estaVacia()){
                System.out.println((i+1)+".- "+horarios[i]);
            }else{
                if(consultorios[pC].cabeza.nPrio!=i){
                System.out.println((i+1)+".- "+horarios[i]);
                }
            }
        }
    }*/
    
    public String getHorarios(int i){
        return horarios[i];
    }
    public int getDia() {
        return dia;
    }
    public void setDia(int dia) {
        this.dia = dia;
    }
    /*public int getContadorDia() {
        return contadorDia;
    }
    public void setContadorDia(int contadorDia) {
        this.contadorDia = contadorDia;
    }*/
    @Override
    public String toString() {
        return "Dias{" ;
    }
}
