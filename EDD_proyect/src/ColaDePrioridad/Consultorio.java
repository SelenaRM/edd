package ColaDePrioridad;


public class Consultorio {
    public Nodo cabeza = null;
    Nodo cola = null;
    Doctor doctor;
    Dias dia[];

    public Consultorio() {
    }

    public Consultorio(Doctor doctor) {
        this.doctor = doctor;
        dia= new Dias[30];
        for (int i = 0; i < 30; i++) {
            dia[i] = new Dias((i + 1));
        }
        
    }
    public void horarios(int pC, int pDia, Consultorio consultorios[]) {
        System.out.println(" -------------- HORARIOS DISPONIBLES -------------");
            Nodo aux=consultorios[pC].buscaDia(pDia);
            if(aux!=null){
                System.out.println("EN EL IF");
                while(aux.prioD==pDia){
                for(int i=0; i<=11;i++){
                    System.out.println("DENTRO DEL FOR DEL WHILE");
                    if(aux.nPrio!=i);
                    System.out.println(i+".- "+dia[pDia].horarios[i]);
                    }
                    aux=aux.siguiente;
                    if(aux==null){
                        break;
                    }
                }
            }else{
                System.out.println("EN EL ELSE");
                for(int i=0; i<12;i++){
                    System.out.println(i+".- "+dia[pDia].horarios[i]);
                }
            }
    }
    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public boolean estaVacia() {
        if (this.cabeza == null) {
            return true;
        } else {
            return false;
        }
    }
    public Nodo buscaDia(int n){
        Nodo aux=cabeza;
        Nodo  auxi = null;
        while(aux!=null){
            if(aux.prioD==n){
                auxi= aux;
                break;
            }
            aux=aux.siguiente;
        }
        return auxi;
    }
    public void insertarCita(Cita cita,int diaP, int nPrm) {// hora
        Nodo nuevoNodo = new Nodo(cita, diaP, nPrm);
        if (estaVacia()) {
            System.out.println("ESTA VACIA");
            cabeza = nuevoNodo;
            cola = nuevoNodo;
        } else {
            if(cabeza.prioD>diaP){
                System.out.println("LA PRIORIDAD DE CABEZA ES MAYOR");
                if (cabeza.nPrio > nPrm) {
                    System.out.println("LA PRIORIDAD DE CABEZA EN DIA Y HORA ES MAYOR");
                    nuevoNodo.siguiente = cabeza;
                    cabeza = nuevoNodo;
                }else {
                    System.out.println("LA HORA DE CABEZA ES MENOR");
                    Nodo aux = null;
                    Nodo route = cabeza;
                    while (route.prioD != diaP) {
                        if(route.nPrio <= nPrm){
                            aux = route;
                            break;
                        }
                        route = route.siguiente;
                    }
                    if (route == cola) {
                        cola = nuevoNodo;
                    }else{
                        nuevoNodo.siguiente = route;
                        aux.siguiente = nuevoNodo;
                    }
                }
            } else{
                System.out.println("LA PRIORIDAD DIA DE CABEZA ES MENOR O IGUAL");
//si la prioridad Dia de cabeza no es mayor busca su lugar en el día y luego en la hora.
                Nodo aux = null;
                Nodo route = cabeza;
                Nodo route2 = cabeza;
                while (route !=  cola && route.prioD <= diaP && route!=null) {
                    System.out.println("BUSCANDO SU LUGAR");
                    if(cabeza.nPrio>nPrm){
                        System.out.println("LA PRIORIDAD DE HORA EN CABEZA ES MAYOR");
                        nuevoNodo.siguiente = cabeza;
                        cabeza = nuevoNodo;
                    }else{//en caso de que no sea mayor pues busca a donde pertecene
                        System.out.println("LA PRIORIDAD DE HORA EN CABEZA ES MENOR O IGUAL ");
                        aux = null;
                        route2 = cabeza;
                        while (route2 != cola ) {
                            if(route2.nPrio <= nPrm){
                                System.out.println("BUSCANDO SU LUGAR.... nPrio");
                                aux = route2;
                                break;
                            }
                            route2 = route2.siguiente;
                        }
                        if (route2 == cola) {
                           cola = nuevoNodo;
                        }else{
                            nuevoNodo.siguiente = route2;
                            aux.siguiente = nuevoNodo;
                        }
                    }
                    route=route.siguiente;
                    if(route==null){
                        break;
                    }
                }
                if(route==cola){if(cabeza.nPrio>nPrm){
                        System.out.println("LA PRIORIDAD DE HORA EN CABEZA ES MAYOR");
                        nuevoNodo.siguiente = cabeza;
                        cabeza = nuevoNodo;
                    }else{//en caso de que no sea mayor pues busca a donde pertecene
                        System.out.println("LA PRIORIDAD DE HORA EN CABEZA ES MENOR O IGUAL ");
                        aux = null;
                        route2 = cabeza;
                        while (route2 != cola ) {
                            if(route2.nPrio <= nPrm){
                                System.out.println("BUSCANDO SU LUGAR.... nPrio");
                                aux = route2;
                                break;
                            }
                            route2 = route2.siguiente;
                        }
                        if (route2 == cola) {
                           cola = nuevoNodo;
                        }else{
                            nuevoNodo.siguiente = route2;
                            aux.siguiente = nuevoNodo;
                        }
                    }
                    
                }
                
            }
        }
    }

    /*public void reasignarNodo(int dat, int nNprim, int nuevoNPrim) {
        Nodo nuevoNodo = new Nodo(dat, nuevoNPrim);
        //Eliminar el dato
        if (cabeza.nPrio == nNprim && cabeza.dat == dat) {
            cabeza = cabeza.siguiente;
        } else {
            Nodo anterior = cabeza;
            Nodo temp = cabeza.siguiente;
            if (temp == cola) {
                cola = anterior;
            }
            while (temp != null) {
                if (temp.dat == dat && temp.nPrio == nNprim) {
                    anterior.siguiente = temp.siguiente;
                    break;
                }
                temp = temp.siguiente;
                anterior = anterior.siguiente;
            }
            if (temp == cola) {
                cola = anterior;
            }
        }
        //Insertar el nuevo nodo
        if (cabeza.nPrio > nuevoNPrim) {
            nuevoNodo.siguiente = cabeza;
            cabeza = nuevoNodo;
        } else {
            Nodo aux_2 = null;
            Nodo route_2 = cabeza;
            while (route_2 != null && route_2.nPrio <= nuevoNPrim) {
                aux_2 = route_2;
                route_2 = route_2.siguiente;
            }
            nuevoNodo.siguiente = route_2;
            aux_2.siguiente = nuevoNodo;
            if (route_2 == null) {
                cola = nuevoNodo;
            }
        }
    }
     */

      /*
    public void eliminarNodo(int numPrioridad) {
        if (estaVacia()) {
            System.out.println("Esta lista esta vacia");
        } else if (cabeza == cola && cabeza.nPrio == numPrioridad) {
            cabeza = cola = null;
        } else if (cabeza.nPrio == numPrioridad) {
            cabeza = cabeza.siguiente;
        } else {
            Nodo anterior = cabeza;
            Nodo temp = cabeza.siguiente;
            while (temp != null && temp.nPrio != numPrioridad) {
                anterior = anterior.siguiente;
                temp = temp.siguiente;
            }
            if (temp != null) {
                anterior.siguiente = temp.siguiente;
                if (temp == cola) {
                    cola = anterior;
                }
            }
        }
    }
     */

    public void imprimir(Consultorio[] array, int pos) {
        Nodo route = cabeza;
        if (route == null) {

        } else {
            while (route != cola && route!=null) {
                System.out.println("->" + route.formato(array,pos));
                route = route.siguiente;
            }
            if(route==cola){
                System.out.println("->" + route.formato(array,pos));
            }
            
        }
    }
}
