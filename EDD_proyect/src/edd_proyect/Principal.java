
import ColaDePrioridad.Cita;
import ColaDePrioridad.Consultorio;
import ColaDePrioridad.Dias;
import ColaDePrioridad.Doctor;

import java.util.Scanner;

public class Principal {
    public static Scanner strings = new Scanner(System.in);
    public static Scanner numeros = new Scanner(System.in);
    public static Consultorio consultorios[] = new Consultorio[4]; // Cambiar el tamaño a 10
    public static Dias dias[] = new Dias[30]; ////////CAMBIÉ EL TAMAÑO A 30
    public static int opc, opc2;

    public static void main(String[] args) {
        medicos();
        do {
            menu();
            opc = numeros.nextInt();

            switch (opc) {
                case 1:
                    insertarCita();
                    break;
                case 2:
                   imprimir();
                   break;
                case 0:
                   System.out.println("Hasta la proxima...");
                   break;
                default:
                   System.out.println("Digita una de las opciones anteriores...");
                   break;
            }
        } while (opc != 0);
    }

    public static void menu() {
        System.out.println();
        System.out.println("Luis Felipe Martinez Ruiz");
        System.out.println("1.- Agregar Cita:----------------------");
        System.out.println("2.- Imprimir Doctores:-----------------");
        System.out.println("0.- Salir del programa: ---------------");
        System.out.println();
    }

       public static void insertarCita() {
        System.out.println("Digita la clave del doctor");
        int claveDoctor = numeros.nextInt();

        for (int i = 0; i < consultorios.length; i++) {
            if (consultorios[i].getDoctor().getClaveDoctor() == claveDoctor) {// verficacion si existe el doctor
                System.out.println("Elejiste al doctor: " + consultorios[i].getDoctor().getNombreDoctor());

                System.out.println("Ingrese la fecha de su cita con el formato correspondiente (dd/mm/yyyy): ");
                System.out.println("Dia:");
                int diaCita = numeros.nextInt();
                Dias dia = new Dias(diaCita);
                System.out.println("Mes");
                int mes = numeros.nextInt();
                System.out.println("Año");
                int anio = numeros.nextInt();     // Verifica fecha y 
                if (ValidaFecha(diaCita, mes)==true) { //Se checa si ese dia es domingo
                    System.out.println("Digita el nombre del paciente");
                    String nombrePaciente = strings.next();
                    System.out.println("Digite la opción correspondiente a la hora que desee");
                    consultorios[i].horarios(i,diaCita,consultorios); //Muestra los horarios disponibles
                    int hora = (numeros.nextInt() - 1);
                    consultorios[i].insertarCita(new Cita(nombrePaciente,diaCita, mes, anio), diaCita, hora);
                }
                break;
            } else {
                System.out.println("No se encuentra este doctor en la lista...");
                break;
            }
        }
    }
    
    public static void medicos() {
        consultorios[0] = new Consultorio(new Doctor("Luis", 1));
        consultorios[1] = new Consultorio(new Doctor("Fernando", 2));
        consultorios[2] = new Consultorio(new Doctor("Jose", 3));
        consultorios[3] = new Consultorio(new Doctor("Diana", 4));
        /*
        consultorios[4] = new Consultorio(new Doctor("Rafael", 5));
        consultorios[5] = new Consultorio(new Doctor("Rodrigo", 6));
        consultorios[6] = new Consultorio(new Doctor("Maximo", 7));
        consultorios[7] = new Consultorio(new Doctor("Abelardo", 8));
        consultorios[8] = new Consultorio(new Doctor("Martin", 9));
        consultorios[9] = new Consultorio(new Doctor("Raul", 10));
         */
    }
    public static boolean ValidaFecha(int d, int m) {
        boolean b=true;
        if (d > 0 && d <= 30) {
            if (d == 6 || d == 13 || d == 20 || d == 27) {
                System.out.println("Dia domingos no se agendan citas");
                b=false;
            } else if (m == 06) {
                System.out.println("Fecha valida");
                b=true;
            } else {
                System.out.println("Lo lamento, sólo agendamos citas del mes de Junio\n" + " .......Volverás al menú de inicio......");
                b=false;
            }
        } else {
            System.out.println("Día no válido, volverás al menú de inicio");
            b=false;
        }
        return b;
    }

  public static void imprimir() {
        System.out.println("-----Doctores-----");
        for (int i = 0; i < consultorios.length; i++) {
            System.out.println(consultorios[i].getDoctor());
            consultorios[i].imprimir(consultorios,i);
            System.out.println("-----------");
        }
    }
}